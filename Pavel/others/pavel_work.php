<?php
$apple = [
    'red',
    'green',
    'red',
    'green',
    'red',
    'green',
    'red',
    'green',
    'red',
    'green',
    'red',
    'green',
];
$green = [];
$red = [];

function myPrint_r($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

foreach ($apple as $item){
    if ($item == 'red') {
        $red[] = $item;
    } else {
        $green[] = $item;
    }
}

myPrint_r($green);
myPrint_r($red);